#PredPrixRBNB

import pandas as pd
import numpy as np
np.random.seed(1)

#Nettoyage

paris_listings = pd.read_csv('paris_airbnb.csv')
paris_listings = paris_listings.loc[np.random.permutation(len(paris_listings))]
stripped_commas = paris_listings['price'].str.replace(',', '')
stripped_dollars = stripped_commas.str.replace('$', '')
paris_listings['price'] = stripped_dollars.astype('float')

drop_columns = ['room_type', 'city', 'state', 'longitude', 'latitude', 'zipcode', 'host_response_rate', 'host_acceptance_rate', 'host_listings_count']
paris_listings = paris_listings.drop(drop_columns, axis=1)

paris_listings = paris_listings.drop(['cleaning_fee', 'security_deposit'], axis=1)
paris_listings = paris_listings.dropna(axis=0)
print(paris_listings.isnull().sum())

#Normalisation
normalized_listings = (paris_listings - paris_listings.mean())/(paris_listings.std())
normalized_listings['price'] = paris_listings['price']


#predictions avec 2 caractéristiques
from sklearn.neighbors import KNeighborsRegressor

train_df = normalized_listings.iloc[0:6000]
test_df = normalized_listings.iloc[6000:]
train_columns = ['accommodates', 'bedrooms']

# instancier le modèle de ML
knn = KNeighborsRegressor(n_neighbors=5, algorithm='brute')

# adapter le modèle aux données
knn.fit(train_df[train_columns], train_df['price'])

# utiliser le modèle pour faire des prédictions
predictions = knn.predict(test_df[train_columns])


#Quantification du resultat
from sklearn.metrics import mean_squared_error

two_features_mse = mean_squared_error(test_df['price'], predictions)
two_features_rmse = two_features_mse**(1/2)

#utiliser 4 caractéristiques

features = ['accommodates', 'bedrooms', 'bathrooms', 'number_of_reviews']
from sklearn.neighbors import KNeighborsRegressor
knn = KNeighborsRegressor(n_neighbors=5, algorithm='brute')
knn.fit(train_df[features], train_df['price'])
four_predictions = knn.predict(test_df[features])
four_mse = mean_squared_error(test_df['price'], four_predictions)
four_rmse = four_mse**(1/2)

print(four_mse)
print(four_rmse)

#utiliser l'ensemble des caractéristiques

knn = KNeighborsRegressor(n_neighbors=5, algorithm='brute')

features = train_df.columns.tolist()
features.remove('price')

knn.fit(train_df[features], train_df['price'])
all_features_predictions = knn.predict(test_df[features])

all_features_mse = mean_squared_error(test_df['price'], all_features_predictions)
all_features_rmse = all_features_mse**(1/2)

print(all_features_mse)
print(all_features_rmse)


